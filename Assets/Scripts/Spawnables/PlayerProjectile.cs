﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectile : Projectile
{
    public Vector2 m_dir;

    protected override void HandleMovement() {
        if(m_dir!=null)
         m_rigidbody2D.velocity = m_dir.normalized * m_speed;
    }

    public override void HandleEffect(GameObject target) {
        target.GetComponent<TowerController>().AddBuff(m_buffType);
    }


}
