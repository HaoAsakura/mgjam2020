﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpController : MonoBehaviour
{
    [SerializeField]
    private Buff m_buff;

    BuffType m_buffType;

    [SerializeField]
    private GameEvent m_pickUpEvent;

    void Awake() {
        GetComponentInChildren<SpriteRenderer>().sprite = m_buff.m_sprite;
        m_buffType = m_buff.m_buffType;

    }

    private void OnTriggerStay2D(Collider2D collision) {
        if(Input.GetKeyDown(KeyCode.Q) && collision.CompareTag(Tags.Player)) {
            collision.GetComponent<PlayerShootController>().m_buffType = m_buffType;
            m_pickUpEvent.Raise();
            Destroy(gameObject);
        }
    }
}
