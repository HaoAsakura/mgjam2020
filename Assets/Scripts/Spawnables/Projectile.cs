﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    [SerializeField]
    public BuffType m_buffType=BuffType.NONE;

    [SerializeField]
    protected float m_speed;

    [SerializeField]
    protected float m_impactDamage;

    [SerializeField]
    protected float m_effectDuration;

    [SerializeField]
    protected float m_lifetime=2.0f;

    [SerializeField]
    public Transform m_target;

    [SerializeField]
    private float m_rotationSpeed;

    protected Rigidbody2D m_rigidbody2D;

    [SerializeField]
    protected string m_collideWith;

    void Start()
    {
        m_rigidbody2D = GetComponent<Rigidbody2D>();
        Invoke("HandleLifetime", m_lifetime);
    }

    void FixedUpdate()
    {
        HandleMovement();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.tag == m_collideWith) {
            HandleEffect(collision.gameObject);
            Destroy(gameObject);
        }
    }

    protected virtual void HandleMovement() {
        if(m_target != null) {
            Vector3 dir = (m_target.position - transform.position).normalized;

            //float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            //Quaternion rotateToTarget = Quaternion.AngleAxis(angle, Vector3.forward);
            //transform.rotation = Quaternion.Slerp(transform.rotation, rotateToTarget, Time.deltaTime * m_rotationSpeed);

            float angle = Vector3.Cross(dir, transform.up).z;
            m_rigidbody2D.angularVelocity = -angle * m_rotationSpeed;
            m_rigidbody2D.velocity = transform.up * m_speed;
        }
    }
        

    public virtual void HandleEffect( GameObject target) {
        target.GetComponent<EnemyController>().TakeDamage(m_impactDamage, m_buffType, m_effectDuration);    
    }

    public void HandleSpawn(Transform target, BuffType buffType) {
        m_target = target;
        m_buffType = buffType;

    }

    private void HandleLifetime() {
        Destroy(gameObject);
    }

}
