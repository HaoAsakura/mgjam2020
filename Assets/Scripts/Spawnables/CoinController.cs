﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    [SerializeField]
    private int m_value;


    private void OnTriggerStay2D(Collider2D collision) {
        if(Input.GetKeyDown(KeyCode.Q) && collision.CompareTag(Tags.Player)) {
            collision.GetComponent<PlayerController>().AddCoins(m_value);
            Destroy(gameObject);
        }
    }
}
