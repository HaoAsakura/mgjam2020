﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public GameEvent onDialogEnding;
    public bool isPlayer;

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue, onDialogEnding);
    }

    public void TriggerSentence()
    {
        int sentenceIndex = Random.Range(0, dialogue.sentences.Length - 1);

        FindObjectOfType<DialogueManager>().StartSentence(dialogue.sentences[sentenceIndex], isPlayer);
    }
}