﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_placeableObjectPrefab;

    private GameObject m_currentPlaceableObject;

    public bool m_canPlace;

    [SerializeField]
    private Color m_canPlaceColor;
    [SerializeField]
    private Color m_canNotPlaceColor;

    [SerializeField]
    private int m_placementCost=1;

    public int m_maxPlaceableTowers=5;

    public int m_currentlyPlacedTowers = 0;

    [SerializeField]
    private int m_UpgradeCost = 1;


    public int m_Coins=3;

    [SerializeField]
    private float m_upgradeRadius = 1.0f;

    [SerializeField]
    private TowerController m_nearTower;


    [SerializeField]
    private GameEvent m_pickUpEvent;


    [SerializeField]
    private GameEvent m_placedTowerEvent;



    void Update()
    {
        HandleNewObjectPlacement();
        
        if(m_currentPlaceableObject != null) {
            MoveCurrentPlaceableObjectToMouse();
            HandleMouseClickIfPlacing();            
        }
        HandleUpgrade();
    }

    private void HandleNewObjectPlacement() {

        if(Input.GetKeyDown(KeyCode.E)) {
            if(m_currentPlaceableObject == null) {
                m_currentPlaceableObject = Instantiate(m_placeableObjectPrefab);
            }
            else {
                Destroy(m_currentPlaceableObject);
                m_currentPlaceableObject = null;
            }
        }
    }

    private void MoveCurrentPlaceableObjectToMouse() {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPos.z = 0;
        m_currentPlaceableObject.transform.position = mouseWorldPos;

        Collider2D[] underMouse = Physics2D.OverlapCircleAll(mouseWorldPos, m_currentPlaceableObject.GetComponent<TowerController>().SpawnMinRange);
        m_canPlace = true;
        foreach(Collider2D obj in underMouse) {
            if(obj.gameObject != m_currentPlaceableObject)
                m_canPlace = false;
        }

        if(m_Coins < m_placementCost)
            m_canPlace = false;
        if(m_currentlyPlacedTowers >= m_maxPlaceableTowers)
            m_canPlace = false;

        if(m_canPlace) {
            m_currentPlaceableObject.GetComponentInChildren<SpriteRenderer>().color = m_canPlaceColor;
        }
        else {
            m_currentPlaceableObject.GetComponentInChildren<SpriteRenderer>().color = m_canNotPlaceColor;
        }

    }

    private void HandleMouseClickIfPlacing() {
        if(Input.GetMouseButtonDown(0)) {
            if(m_canPlace) {
                m_currentPlaceableObject.GetComponent<TowerController>().HandlePlacement();;
                m_Coins = Mathf.Clamp(m_Coins - m_placementCost, 0, m_Coins);
                m_currentPlaceableObject = null;
                m_currentlyPlacedTowers++;
                m_placedTowerEvent.Raise();
                m_pickUpEvent.Raise();
            }
        }
    }

    private void HandleUpgrade() {

        Collider2D[] possibleTargets = Physics2D.OverlapCircleAll(transform.position, m_upgradeRadius);

        float minDist = float.PositiveInfinity;
        m_nearTower = null;
        Transform closestTarget = null;
        foreach(Collider2D nearObjects in possibleTargets) {
            if(nearObjects.tag == Tags.Tower) {
                float dist = Vector3.Distance(nearObjects.transform.position, transform.position);
                if(closestTarget == null) {
                    closestTarget = nearObjects.transform;
                    minDist = dist;
                }
                else {
                    if(dist < minDist) {
                        closestTarget = nearObjects.transform;
                        minDist = dist;
                    }
                }
            }
        }
        if(closestTarget!=null)
            m_nearTower = closestTarget.GetComponent<TowerController>();

        if(Input.GetKeyDown(KeyCode.R) && m_nearTower != null) {
            if(m_Coins>=m_UpgradeCost) {
                m_nearTower.UpgradeTower();
                m_Coins = Mathf.Clamp(m_Coins - m_UpgradeCost, 0, m_Coins);
            }
        }
    }

    public void AddCoins(int value) {
        int coins = Mathf.Clamp(m_Coins + value, 0, m_UpgradeCost);
        if(coins != m_Coins) {
            m_Coins = coins;
            m_pickUpEvent.Raise();
        }
    }

    private void OnDrawGizmos() {
        if(m_currentPlaceableObject != null) {

            Gizmos.DrawWireSphere(m_currentPlaceableObject.transform.position, m_currentPlaceableObject.GetComponent<TowerController>().SpawnMinRange);
        }
    }
}
