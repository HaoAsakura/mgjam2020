﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShootController : MonoBehaviour
{
    public float m_attackSpeed = 1f;

    public GameObject m_projectile;

    public BuffType m_buffType = BuffType.NONE;

    [SerializeField]
    private GameEvent m_pickUpEvent;


    void Update() {
        if(Input.GetMouseButtonDown(0) && CanShoot()) {

            GameObject projectile = Instantiate(m_projectile, transform.position, Quaternion.identity);
            projectile.GetComponent<Projectile>().m_buffType = m_buffType;
            
            Vector3 dir = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized;

            projectile.GetComponent<PlayerProjectile>().m_dir = dir;
            m_buffType = BuffType.NONE;
            m_pickUpEvent.Raise();
        }
    }

    private bool CanShoot() {
        return m_buffType == BuffType.NONE ? false : true;
    }
}
