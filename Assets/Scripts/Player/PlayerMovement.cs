﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float m_speed = 10f;
    private float m_horizontal, m_vertical;

    private Rigidbody2D m_rigidbody2D;


    void Awake()
    {
        m_rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update() {
        m_horizontal = Input.GetAxisRaw("Horizontal");
        m_vertical = Input.GetAxisRaw("Vertical");

        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float rotationZ = Mathf.Atan2(-difference.x, difference.y) * Mathf.Rad2Deg;

        //transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);

    }

    void FixedUpdate()
    {
        Vector2 velocity = new Vector2(m_horizontal, m_vertical);
        m_rigidbody2D.velocity = velocity * m_speed * Time.fixedDeltaTime;
    }
}
