﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour
{
    [SerializeField]
    private float m_attackSpeed = 1.0f;

    [SerializeField]
    private float m_attackRange = 1.0f;

    [SerializeField]
    private float m_spawnMinRange = 1.5f;

    [SerializeField]
    private int m_currentlyActiveBuffs = 0;

    private Color m_defaultColor;
    private float m_attackSpeedTimer;
    public float SpawnMinRange { get => m_spawnMinRange; }

    [SerializeField]
    private GameObject m_projectile;

    [SerializeField]
    private Transform m_currentTarget;

    public bool m_canShoot = false;

    [SerializeField]
    private List<BuffType> m_buffs;
    [SerializeField]
    private List<float> m_buffsDurations;

    [SerializeField]
    private float m_buffsDurationTick;

    [SerializeField]
    private int m_towerBuffsCapacity;

    [SerializeField]
    private float m_BuffDuration=5.0f;

    private TowerBuffDisplay m_towerBuffDisplay;


    void Awake() {
        m_defaultColor = GetComponentInChildren<SpriteRenderer>().color;
        m_towerBuffDisplay = GetComponentInChildren<TowerBuffDisplay>();
        m_buffs = new List<BuffType>();
        m_buffsDurations = new List<float>();
    }

    void Start() {
        StartCoroutine("HandleBuffDuration");
    }

    void Update() {
         m_attackSpeedTimer -= Time.deltaTime;
        if(m_canShoot && m_attackSpeedTimer <= 0) {
            m_attackSpeedTimer = m_attackSpeed;
            FindTarget();
            if(m_currentTarget != null) {
                BuffType combinedBuff = BuffType.NONE;
                foreach(BuffType buff in m_buffs) { combinedBuff |= buff; }
                if(combinedBuff.HasFlag(BuffType.FIRE) && combinedBuff.HasFlag(BuffType.SHOCK)) {
                    HandleExplosion();
                    return;
                }

                GameObject projectile = Instantiate(m_projectile, transform.position, Quaternion.identity);
                projectile.GetComponent<Projectile>().HandleSpawn(m_currentTarget, combinedBuff);
            }
        }

    }

    public void HandlePlacement() {
        GetComponentInChildren<SpriteRenderer>().color = m_defaultColor;
        GetComponent<Collider2D>().isTrigger = false;
        m_canShoot = true;
    }

    private void FindTarget(){
        Collider2D[] possibleTargets = Physics2D.OverlapCircleAll(transform.position, m_attackRange);

        float minDist = float.PositiveInfinity;
        Transform closestTarget = null;
        foreach(Collider2D target in possibleTargets) {
            if(target.tag == Tags.Enemy) {
                float dist = Vector3.Distance(target.transform.position, transform.position);
                if(closestTarget == null) {
                    closestTarget = target.transform;
                    minDist = dist;
                }    
                else {
                    if(dist < minDist) {
                        closestTarget = target.transform;
                        minDist = dist;
                    }
                }
            }
        }
        m_currentTarget = closestTarget;
    }

    public void AddBuff(BuffType buff) {
        if(m_buffs.Count >= m_towerBuffsCapacity) {
            m_buffs.RemoveAt(0);
            m_buffsDurations.RemoveAt(0);
        }

        m_buffs.Add(buff);
        m_buffsDurations.Add(m_BuffDuration);
        m_towerBuffDisplay.UpdateBuffsUI(m_buffs);
    }


    public void HandleExpiredBuffs() {
        for(int i = m_buffsDurations.Count-1; i >= 0; i--) {
            if(m_buffsDurations[i] <= 0) {
                m_buffs.RemoveAt(i);
                m_buffsDurations.RemoveAt(i);
                
            }
        }
        m_towerBuffDisplay.UpdateBuffsUI(m_buffs);
    }

    IEnumerator HandleBuffDuration() {
        while(true) {
            for(int i = 0; i < m_buffsDurations.Count; i++) {
                m_buffsDurations[i] -= m_buffsDurationTick;
            }
            HandleExpiredBuffs();
            yield return new WaitForSeconds(m_buffsDurationTick);
        }

    }

    public void UpgradeTower() { }

    public void HandleExplosion() {
        m_canShoot = false;
        Collider2D[] possibleTargets = Physics2D.OverlapCircleAll(transform.position, m_attackRange);

        foreach(Collider2D target in possibleTargets) {
            if(target.tag == Tags.Enemy) {
                target.GetComponent<EnemyController>().TakeDamage(10000.0f, BuffType.NONE, 0.0f);
            }
        }
        Destroy(gameObject);
    }

    private void OnDrawGizmos() {

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,m_attackRange);
        
    }
}
