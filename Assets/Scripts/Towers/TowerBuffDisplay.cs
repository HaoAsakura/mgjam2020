﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBuffDisplay : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer[] m_buffs;

    [SerializeField]
    private Sprite[] m_sprites;

    public void UpdateBuffsUI(List<BuffType> buffTypes) {
        int i = 0;
        for(; i < buffTypes.Count; i++) { 
            switch(buffTypes[i]) {
                case BuffType.NONE:
                    m_buffs[i].sprite = null;
                break;

                case BuffType.FIRE:
                    m_buffs[i].sprite = m_sprites[0];
                    break;

                case BuffType.ICE:
                    m_buffs[i].sprite = m_sprites[1];
                    break;

                case BuffType.SHOCK:
                    m_buffs[i].sprite = m_sprites[2];
                    break;
            }
        }
        for(; i < m_buffs.Length; i++) {
            m_buffs[i].sprite = null;
        }
    }
}
