﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreController : MonoBehaviour
{

    [SerializeField]
    private float m_hp=10.0F;

    [SerializeField]
    GameEvent m_DamageTaken;

    [SerializeField]
    GameEvent m_CoreDestroyed;

    [SerializeField]
    private bool m_alive = true;

    public void TakeDamage(float damage) {
        m_hp -= damage;
        m_DamageTaken.Raise();
        if(m_hp <= 0 && m_alive) {
            HandleDeath();
            m_alive = false;
        }   
    }

    private void HandleDeath() {
        m_CoreDestroyed.Raise();
        //Destroy(gameObject);
    }
}
