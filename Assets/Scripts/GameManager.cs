﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private bool m_gameStarted = false;
    
    public DialogueTrigger startDialogTrigger;
    public DialogueTrigger playerDialogsTrigger;
    public DialogueTrigger enemiesDialogsTrigger;

    private const float DIALOG_POPUP_DELAY = 15f;

    [SerializeField]
    private float timer = 0f;

    [SerializeField]
    EnemySpawnerManager spawnerManager;

    void Start()
    {
        startDialogTrigger.TriggerDialogue();
    }

    void Update()
    {
        if(m_gameStarted)
            CheckSentenceTriggering();
    }

    void CheckSentenceTriggering()
    {
        timer += Time.deltaTime;

        if(timer >= DIALOG_POPUP_DELAY)
        {
            Debug.Log("Check sentence triggering");
            int rand = Random.Range(0, 1000);
            bool triggered = false;

            if (rand % 4 == 0)
            {
                playerDialogsTrigger.TriggerSentence();
                triggered = true;
            }
            else if (rand % 3 == 0)
            {
                enemiesDialogsTrigger.TriggerSentence();
                triggered = true;
            }

            if(triggered)
            {
                Debug.Log("Triggered");
                timer = 0f;
            }
        }
    }

    public void StartGame()
    {
        m_gameStarted = true;
        spawnerManager.StartWave(1);
    }
}
