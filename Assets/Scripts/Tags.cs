﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tags
{
    public const string Player = "Player";
    public const string Enemy = "Enemy";
    public const string Projectile = "Projectile";
    public const string Tower = "Tower";
    public const string PowerUp = "Powerup";
    public const string Core = "Core";
    public const string Spawner = "Spawner";
    public const string SpawnerManager = "SpawnerManager";
}
