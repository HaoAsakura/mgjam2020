﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    private const float ON_SCREEN_TIME = 3f;
    private bool checkOnScreenTime = false;
    private float timer = 0f;

    public Text nameText;
    public Text dialogueText;
    public GameObject continueButton;

    public Animator animator;
    [SerializeField]
    private Queue<string> sentences = new Queue<string>();

    private GameEvent m_onDialogEnding;

    void Update()
    {
        if (checkOnScreenTime)
            CheckOnScreenTime();
    }

    private void CheckOnScreenTime()
    {
        timer += Time.deltaTime;

        if(timer >= ON_SCREEN_TIME)
        {
            EndDialogue();
            timer = 0f;
            checkOnScreenTime = false;
        }
    }

    // Update is called once per frame
    public void StartDialogue(Dialogue dialogue, GameEvent onDialogEnding = null)
    {
        m_onDialogEnding = onDialogEnding;
        continueButton.SetActive(true);

        animator.SetBool("IsOpen", true);
        nameText.text = dialogue.name;
        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void StartSentence(string sentence, bool isPlayer)
    {
        m_onDialogEnding = null;
        continueButton.SetActive(false);

        animator.SetBool("IsOpen", true);
        dialogueText.text = sentence;
        Debug.Log("text: " + sentence);

        checkOnScreenTime = true;
    }

    public void DisplayNextSentence()
    {
        Debug.Log("Display next sequence");
        if(sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        Debug.Log("text: "+ sentence);
        dialogueText.text = sentence;
    }
    
    void EndDialogue()
    {
        animator.SetBool("IsOpen", false);
        sentences.Clear();

        if (m_onDialogEnding != null)
            m_onDialogEnding.Raise();
    }
}
