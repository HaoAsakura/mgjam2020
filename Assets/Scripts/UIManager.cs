﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static int RESOURCES_MAX_COUNT = 20;
    public static int NUCLEUS_MAX_HEALTH = 10;

    public static int NEW_TOWER_COST = 1;

    public GameObject player;

    public Image currentNucleusHealth;

    public Image currentBuffType;
    public Sprite fireIcon;
    public Sprite iceIcon;
    public Sprite shockIcon;

    public RectTransform currentResourcesBar;
    private float currentResourcesBarHeight;
    public RawImage currentResourcesImage;

    public Text m_placedTowersText;

    public GameEvent placedTower;
    public GameEvent gameEnded;

    private PlayerController m_playerController;

    public GameObject m_gameOverCanvas;


    private void Awake() {
        m_playerController = GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>();
    }

    void Start()
    {
        currentResourcesBarHeight = currentResourcesBar.sizeDelta.y;
    }

    void Update()
    {
        Rect resourcesUvRect = currentResourcesImage.uvRect;
        resourcesUvRect.y += Time.deltaTime * .2f;
        currentResourcesImage.uvRect = resourcesUvRect;
    }

    public void DecreaseNucleusHealth(float health)
    {
        currentNucleusHealth.fillAmount -= health;
    }

    public void UpdateResourcesCount()
    {
        int coins = player.GetComponent<PlayerController>().m_Coins;
        Vector2 barSize = currentResourcesBar.sizeDelta;
        coins = (200 * coins) / 3;
        barSize.y = Mathf.Clamp(coins,0,600);
        currentResourcesBar.sizeDelta = barSize;
    }

    public void UpdateBuffOwned()
    {
        BuffType type = player.GetComponent<PlayerShootController>().m_buffType;
        
        switch(type)
        {
            case BuffType.FIRE:
                currentBuffType.enabled = true;
                currentBuffType.sprite = fireIcon;
                break;
            case BuffType.ICE:
                currentBuffType.enabled = true;
                currentBuffType.sprite = iceIcon;
                break;
            case BuffType.SHOCK:
                currentBuffType.enabled = true;
                currentBuffType.sprite = shockIcon;
                break;
            default:
                currentBuffType.enabled = false;
                currentBuffType.sprite = null;
                break;
        }

        if (currentBuffType.enabled)
            currentBuffType.preserveAspect = true;
    }

    public void UpdateTowersPlaced() {
        m_placedTowersText.text = m_playerController.m_currentlyPlacedTowers + "/" + m_playerController.m_maxPlaceableTowers;
    }

    public void HandleGameOver() {
        StopCoroutine("HandleGameOverDuration");
        StartCoroutine("HandleGameOverDuration");
    }

    IEnumerator HandleGameOverDuration() {
        m_gameOverCanvas.SetActive(true);
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                

    }
}
