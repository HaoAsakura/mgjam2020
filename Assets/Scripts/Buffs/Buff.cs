﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Flags]public enum BuffType { NONE = 0, FIRE = 1, ICE = 2, SHOCK = 4 }

[CreateAssetMenu(fileName = "Buff", menuName = "Buff", order = 1)]
public class Buff : ScriptableObject
{
    public BuffType m_buffType;
    public Sprite m_sprite;

    public void HandleEffect() { }


}
