﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    [SerializeField]
    private float m_spawnTime;

    [SerializeField]
    public bool m_spawnActive=false;

    [SerializeField]
    private GameObject m_spawnable;

    private BoxCollider2D[] m_areas;

    void Start()
    {
        m_areas = GetComponentsInChildren<BoxCollider2D>();
        StartCoroutine("Spawn");
    }

    private IEnumerator Spawn() {

        while(m_spawnActive) {

            Collider2D randomArea = m_areas[Random.Range(0, m_areas.Length)];
            Vector3 spawnCenter = randomArea.bounds.center;
            Vector3 spawnArea = randomArea.bounds.extents;

            float x = Random.Range(spawnCenter.x - spawnArea.x, spawnCenter.x + spawnArea.x);
            float y = Random.Range(spawnCenter.y - spawnArea.y, spawnCenter.y + spawnArea.y);

            GameObject obj = Instantiate(m_spawnable, new Vector3(x, y, 0.0f), Quaternion.identity);


            yield return new WaitForSeconds(m_spawnTime);
        }

    }

    public void ActivateSpawner() {
        m_spawnActive = true;
        StopCoroutine("Spawn");
        StartCoroutine("Spawn");

    }

    public void DeactivateSpawner() {
        m_spawnActive = false;
        StopCoroutine("Spawn");
    }
}
