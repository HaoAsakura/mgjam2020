﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    [SerializeField]
    private float m_hp=5;

    [SerializeField]
    private float m_attackDamage = 1.0f;

    [SerializeField]
    private float m_attackSpeed = 1.0f;


    private float m_attackSpeedTimer = 0.0f;

    [SerializeField, Range(0,1)]
    private float m_dropChance;

    [SerializeField]
    List<GameObject> m_buffs;

    [SerializeField]
    private int m_coinAmount;

    [SerializeField]
    private GameEvent m_deathEvent;

    public BuffType m_status;

    [SerializeField]
    private float m_statusDurationTick;

    [SerializeField]
    private float m_statusRadius=3.0f;

    private float m_statusDuration;

    private bool m_isBurning=false;

    private ParticleSystem m_statusParticleSystem;

    [SerializeField]
    private Sprite[] m_statusSprites;

    private CoreController m_coreController;

    private void Awake() {
        m_statusParticleSystem = GetComponentInChildren<ParticleSystem>();
        m_coreController = GameObject.FindGameObjectWithTag(Tags.Core).GetComponent<CoreController>();
    }


    public void TakeDamage(float damage, BuffType status, float statusDuration) {
        m_hp -= damage;
        m_status |= status;
        HandleEffect(statusDuration);
        if(m_hp <= 0)
            HandleDeath();
    }

    private void HandleDeath() {
        GameObject.FindGameObjectWithTag(Tags.Player).GetComponent<PlayerController>().AddCoins(m_coinAmount);
        HandleDrop();
        m_deathEvent.Raise();
        Destroy(gameObject);
    }

    private void HandleDrop() {
        
        if(m_buffs.Count>0 && Random.value <= m_dropChance) {
            Instantiate(m_buffs[Random.Range(0,m_buffs.Count)], transform.position,Quaternion.identity);
        }

    }

    private void OnTriggerStay2D(Collider2D collision) {
        if(m_isBurning && collision.CompareTag(Tags.Enemy) && !collision.GetComponent<EnemyController>().m_status.HasFlag(BuffType.FIRE)) {
            collision.GetComponent<EnemyController>().TakeDamage(0.0f, BuffType.FIRE, m_statusDuration);
        }

        m_attackSpeedTimer -= Time.deltaTime;
        if(collision.CompareTag(Tags.Core) && m_attackSpeedTimer <= 0) {
            m_attackSpeedTimer = m_attackSpeed;
            m_coreController.TakeDamage(m_attackDamage);
        }
    }

    public void HandleEffect(float statusDuration) {
        if(m_status.HasFlag(BuffType.FIRE))
            StartCoroutine("FireEffect", statusDuration);
        if(m_status.HasFlag(BuffType.ICE))
            StartCoroutine("IceEffect", statusDuration);
        if(m_status.HasFlag(BuffType.SHOCK))
            StartCoroutine("ShockEffect", statusDuration);
        if(m_status.HasFlag(BuffType.FIRE) && m_status.HasFlag(BuffType.ICE))
            FireIceEffect();
    }

    IEnumerator FireEffect(float statusDuration) {
        m_statusDuration = statusDuration;
        m_isBurning = true;
        m_statusParticleSystem.textureSheetAnimation.SetSprite(0,m_statusSprites[0]);
        m_statusParticleSystem.Play();

        yield return new WaitForSeconds(statusDuration);

        m_isBurning = false;
        m_statusParticleSystem.Stop();
        HandleDeath();
    }

    IEnumerator IceEffect(float statusDuration) {
        GetComponent<EnemyMovement>().m_canMove = false;
        m_statusParticleSystem.textureSheetAnimation.SetSprite(0, m_statusSprites[1]);
        m_statusParticleSystem.Play();
        Collider2D[] possibleTargets = Physics2D.OverlapCircleAll(transform.position, m_statusRadius);
        
        foreach(Collider2D target in possibleTargets) {
            if(target.CompareTag(Tags.Enemy) && target.gameObject != gameObject) {
                target.GetComponent<EnemyMovement>().StartCoroutine("Slowed", statusDuration);
            }
        }
        yield return new WaitForSeconds(statusDuration);

        m_statusParticleSystem.Stop();
        GetComponent<EnemyMovement>().m_canMove = true;
        m_status &= ~BuffType.ICE; //remove the ice status;
    }

    IEnumerator ShockEffect(float statusDuration) {
        if(statusDuration > 0) {
            m_statusParticleSystem.textureSheetAnimation.SetSprite(0, m_statusSprites[2]);
            m_statusParticleSystem.Play();
            GetComponent<EnemyMovement>().m_canMove = false;
            Collider2D[] possibleTargets = Physics2D.OverlapCircleAll(transform.position, m_statusRadius);

            foreach(Collider2D target in possibleTargets) {
                if(target.CompareTag(Tags.Enemy) && target.gameObject != gameObject && !target.GetComponent<EnemyController>().m_status.HasFlag(BuffType.SHOCK)) {
                    target.GetComponent<EnemyController>().TakeDamage(0.0f, BuffType.SHOCK, statusDuration-1);
                    print(gameObject.name +" - "+ target.name);
                    break;
                }
            }
            yield return new WaitForSeconds(statusDuration);
            m_statusParticleSystem.Stop();
            GetComponent<EnemyMovement>().m_canMove = true;
        }
        m_status &= ~BuffType.SHOCK; //remove the shock status;

    }

    private void FireIceEffect() { }

    private void FireShockEffect() { }

}
