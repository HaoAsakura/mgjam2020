﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    private float m_speed = 10f;

    public bool m_canMove  = true;

    [SerializeField]
    private Transform m_target;

    private float m_horizontal, m_vertical;

    private Rigidbody2D m_rigidbody2D;

    public bool m_slowed;

    void Awake() {
        m_rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Start() {
        m_target = GameObject.FindGameObjectWithTag(Tags.Core).transform;
    }

    void Update() {
        m_horizontal = Input.GetAxisRaw("Horizontal");
        m_vertical = Input.GetAxisRaw("Vertical");

        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        //float rotationZ = Mathf.Atan2(-difference.x, difference.y) * Mathf.Rad2Deg;

        //transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);

    }

    void FixedUpdate() {
        if(m_target != null && m_canMove) {
            Vector3 dir = m_target.position - transform.position;

            //float rotationZ = Mathf.Atan2(-dir.x, dir.y) * Mathf.Rad2Deg;
            //transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ);

            //Vector2 velocity = new Vector2(m_horizontal, m_vertical);
            m_rigidbody2D.velocity = dir.normalized * (m_slowed ? m_speed * 0.5f : m_speed) * Time.fixedDeltaTime;
        }
        else {
            m_rigidbody2D.velocity = Vector2.zero;

        }

    }

    public IEnumerator Slowed(float statusDuration) {
        if(!m_slowed) {
            m_slowed = true;
            float originalSpeed = m_speed;
            m_speed *= 0.5f;
            yield return new WaitForSeconds(statusDuration);
            m_speed = originalSpeed;
            m_slowed = false;
        }
    }
}
