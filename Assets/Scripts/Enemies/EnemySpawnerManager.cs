﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawnerManager : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> m_spawners;

    [SerializeField]
    private int m_wavesNumber=1;

    [SerializeField]
    private int m_concurrentlyActiveSpawners= 1;

    [SerializeField]
    private float m_waveDuration=30.0f;

    [SerializeField]
    private float m_timeBetweenWaves=5.0f;

    [SerializeField]
    private float m_waveTick = .5f;

    private bool m_wavePhase = false;
    private bool m_restPhase = true;

    void Start()
    {

        m_spawners = GameObject.FindGameObjectsWithTag(Tags.Spawner).ToList();
        //StartCoroutine("RestPhase");
    }

    private IEnumerator WavePhase() {
        m_wavePhase = true;
        if(m_concurrentlyActiveSpawners < m_spawners.Count) {
            List<int> alreadyActivate = new List<int>();
            for(int i = 0; i < m_concurrentlyActiveSpawners; i++) {
                int spawnerIndex;
                do {
                    spawnerIndex = Random.Range(0, m_spawners.Count);
                } while(alreadyActivate.Contains(spawnerIndex));

                m_spawners[spawnerIndex].GetComponent<EnemySpawner>().ActivateSpawner();
                alreadyActivate.Add(spawnerIndex);
            }
        }
        else {
            for(int i = 0; i < m_spawners.Count; i++) {
                m_spawners[i].GetComponent<EnemySpawner>().ActivateSpawner();
            }
        }

        yield return new WaitForSeconds(m_waveDuration);

        for(int i = 0; i < m_spawners.Count; i++) {
            m_spawners[i].GetComponent<EnemySpawner>().DeactivateSpawner();
        }
        m_wavePhase = false;
        StopCoroutine("RestPhase");
        StartCoroutine("RestPhase");

        //while(m_wavePhase) {
        //    yield return new WaitForSeconds(m_waveTick);
        //}
    }

    private IEnumerator RestPhase() {
        m_restPhase = true;

        yield return new WaitForSeconds(m_timeBetweenWaves);

        m_restPhase = false;

        StopCoroutine("WavePhase");
        StartCoroutine("WavePhase");
    }

    public void StartWave(int activeSpawners) {
        m_concurrentlyActiveSpawners = activeSpawners;
        StopCoroutine("WavePhase");
        StartCoroutine("WavePhase");
    }
}
